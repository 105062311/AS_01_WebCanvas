
var canvas, ctx, flag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0,
    startX = 0,
    startY = 0,
    cStep = -1,
    dataURL,
    dot_flag = false;

var cPushArray = new Array();    
var mode = 'pen';
var textsize;

var x = "black",
    y = 1;
 
function init() {
    canvas = document.getElementById('can');
    ctx = canvas.getContext("2d");
    w = canvas.width;
    h = canvas.height;

 
    ctx.fillStyle="white";
    ctx.fillRect(0, 0, w, h);
    cPush(); 

    canvas.addEventListener("mousemove", function (e) {
        findxy('move', e)
    }, false);
    canvas.addEventListener("mousedown", function (e) {
        findxy('down', e)
    }, false);
    canvas.addEventListener("mouseup", function (e) {
        findxy('up', e)
    }, false);
    canvas.addEventListener("mouseout", function (e) {
        findxy('out', e)
    }, false);
}

function modechoose(obj) {
    mode = obj;
}

function color(obj) {
    x = obj;
}

function range(obj) {
    y = obj;
}

function draw() {
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(currX, currY);
    switch (mode) {
        case 'pen':
            ctx.strokeStyle = x;
            break;
        case 'eraser':
            ctx.strokeStyle = "white";
            break;
    }
    ctx.lineWidth = y;
    ctx.stroke();
    ctx.closePath();
}

function text() {
    textsize = y / 0.2 + 10;
    var T = document.getElementById("inputtext").value;
    var F = document.getElementById("fonttext").value;
    ctx.font = textsize.toString() + "px " + F;
    ctx.fillStyle = x;
    ctx.textAlign = "center";
    ctx.fillText(T, currX, currY);
}

function circle() {
    ctx.moveTo(prevX, prevY);
    ctx.beginPath();
    ctx.arc(startX, startY, Math.abs(currX - startX), 0, Math.PI * 2);
    ctx.lineWidth = y / 4;
    ctx.fillStyle = x;
    ctx.strokeStyle = x;
    ctx.stroke();
    ctx.closePath();
}

function rectangle() {
    ctx.moveTo(prevX, prevY);
    ctx.beginPath();
    ctx.lineWidth = y / 4;
    ctx.strokeStyle = x;
    ctx.rect(startX, startY, currX - startX, currY - startY);
    ctx.stroke();
    ctx.closePath();
}

function triangle() {
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineWidth = y / 4;
    ctx.strokeStyle = x;
    ctx.lineTo(currX, currY);
    ctx.lineTo(2 * startX - currX, currY);
    ctx.closePath();
    ctx.stroke();
    /*fill-->fillStyle()且不需要closePath 最後fill();*/
}


function line(){
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineWidth = y / 4;
    ctx.strokeStyle = x;
    ctx.lineTo(currX, currY);
    ctx.closePath();
    ctx.stroke();
}

function erase() {
    var m = confirm("Want to clear");
    if (m) {
        ctx.clearRect(0, 0, w, h);
        ctx.fillStyle="white";
        ctx.fillRect(0, 0, w, h);
        cPush();
    }
}

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(canvas.toDataURL());
}

function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function save() {
    dataURL = canvas.toDataURL();
}

function load() {
    var imageObj = new Image();
    imageObj.onload = function () {
        ctx.clearRect(0, 0, w, h);
        ctx.drawImage(imageObj, 0, 0);
    }
    imageObj.src = dataURL;
}

function readImage(input) {
    if ( input.files && input.files[0] ) {
      var FR= new FileReader();
      FR.onload = function(e) {
        //e.target.result = base64 format picture
        $('#img').attr( "src", e.target.result );
      };       
      FR.readAsDataURL( input.files[0] );
    }
}

function drawimage(){
    var image = document.getElementById('img')
    ctx.drawImage(image, currX, currY );
}

function findxy(res, e) {
    if (res == 'down') {
        prevX = currX;
        prevY = currY;
        currX = e.clientX - canvas.offsetLeft;
        currY = e.clientY - canvas.offsetTop;
        startX = currX;
        startY = currY;
        save();
        switch (mode) {
            case 'pen':
            case 'eraser':
                flag = true;
                dot_flag = true;
                
                if (dot_flag) {
                    ctx.beginPath();
                    ctx.fillStyle = x;
                    ctx.fillRect(currX, currY, 2, 2);
                    ctx.closePath();
                    dot_flag = false;
                }
                break;
            case 'line':
            case 'circle':
            case 'rectangle':
            case 'triangle':
                flag = true;
                dot_flag = true;
                break;
            case 'drawimage':
                flag = true;
                dot_flag = true;
                drawimage();
                break;
        }

    }
    if (res == 'up' || res == 'out') {
        flag = false;
        switch (mode) {
            case 'pen':
            case 'eraser':
            case 'drawimage':
                if(res == 'up')cPush(); 
                break;
            case 'text':
                text();
                if(res == 'up')cPush(); 
                break;
            case 'circle':
                circle();
                cPush(); 
                break;
            case 'rectangle':
                rectangle();
                cPush(); 
                break;
            case 'triangle':
                triangle();
                cPush(); 
                break;
            case 'line':
                line();
                cPush(); 
                break;
            
        }
    }
    if (res == 'move') {
        if (flag) {
            prevX = currX;
            prevY = currY;
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
            switch (mode) {
                case 'pen':
                case 'eraser':
                    draw();
                    break;
                case 'circle':
                    load();
                    circle();
                    break;
                case 'rectangle':
                    load();
                    rectangle();
                    break;
                case 'triangle':
                    load();
                    triangle();
                    break;
                case 'line':
                    load();
                    line();
                    break;
            }

        }
    }
}

