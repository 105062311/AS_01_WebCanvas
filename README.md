# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## REPORT
**JavaScript**
* init 
    ->偵測滑鼠的按下、移動、放開以及移出canvas來將該行動傳入function findxy
* modechoose(obj)
    ->當畫筆、橡皮擦等按鈕被按下後，傳入字串來切換現在的mode
* color(obj)
    ->調色盤input顏色後，將值傳入color，以此可以選擇畫筆文字等等的顏色
* range(obj)
    ->滑動桿input大小後，將值傳入range，以此可以選擇畫筆文字等等的大小
* draw
    ->將畫筆從上個位置連直線到此時畫筆的位置，若mode為橡皮擦則將顏色設定成白色
* text
    ->用getElementById將HTML的input中獲取要輸入的文字以及字型後，再將
    滑動桿的數字經過調整後轉成字串丟入font的設定
* circle
    ->startX,startY為滑鼠按下時的位置，將圓心設在startX跟startY，接著
    以currX-startX作為半徑畫圓
* rectangle
    ->將起始點設在startX跟startY，接著以currX-startX作為寬，以currY-startY作為高畫長方形
* triangle
    ->將畫筆位置設成(startX,startY)，接著畫出往(currX,currY)的直線，
    同時對稱畫出通往(2*startX-currX,currY)的線，用closePath()來連接兩條線的末段形成三角形
* line
    ->將畫筆移到(startX,startY)後往(currX,currY)畫直線
* erase
    ->讓使用者確認是否清除所有圖案後將其重新清除以及畫上白底
* cPush
    ->每完成一筆時呼叫，利用cStep來控制Array內的圖片，並將每一張圖push進cPushArray內
* cUndo
    ->當cStep>0時代表先前已push過圖片進去，需將cStep-1後，下一次Undo才可以取到更上一張圖
    ->從cPushArray取到的圖load回canvas中
* cRedo
    ->當cStep<cPushArray.length-1時代表沒有超過目前所擁有的圖片數，並把cStep+1表示跳到下一張圖
    ->從cPushArray取到的圖load回canvas中
* save、load
    ->畫圓形三角形長方形時，每次滑鼠down時都將圖片save起來，在move時先load該原始圖片再按照當前
    滑鼠位置畫出一個圖形來，避免在canvas畫出滑鼠移動過程的圖案
* readImage(input)
    ->當input file有圖片輸入後，將img的src設定為該圖片位置，預覽於網頁上
* drawImage
    ->用getElementById將img抓取下來後畫於canvas上
* findxy(res, e)    
    ->設定prev、curr、strat滑鼠的位置的值，並針對不同的滑鼠動作以及mode來呼叫不同function



## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


